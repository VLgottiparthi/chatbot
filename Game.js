const GameState = Object.freeze({
  WELCOMING: Symbol("welcome"),
  START: Symbol("start"),
  QUIZ: Symbol("quiz"),
  VERIFYANSWER: Symbol("verifyanswer"),
  END: Symbol("end"),
});

const QuizJSON = require("./www/quiz.json");

const TOTALQSINQUIZ = 10;

function Game() {
  this.currentState = GameState.WELCOMING;
  this.currentCount = 0;
  this.askedQuestionIDObj = {};
  this.currentQuestion = null;
  this.currentPlayer = 1;
  this.currentScores = [0,0];
}

function getRandomNumberUpto(n) {
  return Math.ceil(Math.random() * n);
}

Game.prototype.quiz = function () {
  let randomQuestionID;
  do {
    randomQuestionID = getRandomNumberUpto(10);
  } while (this.askedQuestionIDObj[randomQuestionID]);

  this.askedQuestionIDObj[randomQuestionID] = true;
  this.currentCount++;
  return randomQuestionID;
};

Game.prototype.setInitialState = function () {
  this.currentCount = 0;
  this.askedQuestionIDObj = {};
  this.currentQuestion = null;
  this.currentPlayer = 1;
  this.currentScores = [0, 0];
};

Game.prototype.makeAMove = function (sInput) {
  let sReply = "";

  switch (this.currentState) {
    case GameState.WELCOMING:
      sReply = `Hello, I am a two player quiz chatbot. 
        The quiz will be held for player 1 & 2 based out of the tv series Game of thrones. 
        Type \"start\" to begin`;
      this.currentState = GameState.START;
      break;

    case GameState.START:
      this.setInitialState();
      if (sInput && sInput.toLowerCase().match("start")) {
        this.currentState = GameState.QUIZ;
        sReply = `The quiz is started. 
        Please follow the below instructions:
        There would be 4 options for each question.
        The player type alphabet of option to submit the answer.
        E.g if first option is correct, type "A" and send.

        Type & send any key to start the quiz`;
      } else {
        sReply = 'Type "start" to begin';
      }
      break;

    case GameState.QUIZ:
      if (this.currentCount === TOTALQSINQUIZ) {
        this.currentState = GameState.END;
        break;
      }
      const questionId = this.quiz();
      this.currentQuestion = questionId;

      sReply = `${QuizJSON[questionId].question}
            A)  ${QuizJSON[questionId].options[0]}
            B)  ${QuizJSON[questionId].options[1]}
            C)  ${QuizJSON[questionId].options[2]}
            D)  ${QuizJSON[questionId].options[3]}
            Player 1, please enter your choice
          `;
      this.currentState = GameState.VERIFYANSWER;
      break;

    case GameState.VERIFYANSWER:
      const arr = ["A", "a", "B", "b", "C", "c", "D", "d"];
      if (arr.indexOf(sInput) === -1) {
        sReply = "Invalid option. Please select any option from A,B,C or D";
        break;
      }

      if (sInput.toUpperCase() === QuizJSON[this.currentQuestion].answer) {
        if (this.currentPlayer === 1) {
          this.currentScores[0]++;
        } else {
          this.currentScores[1]++;
        }
      }
      if (this.currentPlayer === 2) {
        this.currentPlayer = 1;
        this.currentState = GameState.QUIZ;
        if (this.currentCount === TOTALQSINQUIZ) {
          this.currentState = GameState.END;
        }
        sReply = this.makeAMove();
        break;
      }
      if (this.currentPlayer === 1) {
        this.currentPlayer = 2;
      }
      sReply = `Player ${this.currentPlayer}, Please enter your choice`;
      break;

    case GameState.END:
      const player1Score = this.currentScores[0];
      const player2Score = this.currentScores[1];
      const str1 = `The quiz is ended. 
      Player 1 scored ${player1Score} 
      Player 2 scored ${player2Score}`;
      let str2 = "";
      if (player1Score > player2Score) {
        str2 = "Player 1 wins !!!";
      } else if (player1Score < player2Score) {
        str2 = "Player 2 wins !!!";
      } else {
        str2 = "It's a draw";
      }
      const str3 = `Type "start" to restart again`;

      sReply = `${str1} 
          ${str2}
          ${str3}`;
      if (sInput && sInput.toLowerCase().match("start")) {
        this.currentState = GameState.QUIZ;
        sReply = this.makeAMove();
      }
      break;

    default:
      sReply = "Please enter valid input";
  }
  console.log("sreply", sReply);
  return [sReply];
};

module.exports = Game;
